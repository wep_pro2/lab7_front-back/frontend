import { ref } from 'vue'
import { defineStore } from 'pinia'
import axios from 'axios'
import http from '@/services/http'
import { useLoadingStore } from './loading'
import temperatureService from '@/services/temperature'

export const useTemperatureStore = defineStore('temperature', () => {
  const valid = ref(false)
  const celsius = ref(0)
  const result = ref(0)
  const loadingStore = useLoadingStore()
  type ReturnData = {
    celsius: number
    fahrenheit: number
  }

  async function callconvert() {
    console.log('store: call Convert')
    loadingStore.doLoad()
    try {
      result.value = await temperatureService.convert(celsius.value)
    } catch (e) {
      console.log(e)
    }
    loadingStore.finish()
    console.log('store: Finish call Convert')
  }
  return { valid, result, celsius, callconvert }
})
