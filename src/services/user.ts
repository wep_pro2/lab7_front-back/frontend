import type { User } from "@/types/User";
import http from "./http";

function addUser(user: User) {
    return http.post('/users', user)
}

function updateUser(user: User) {
    return http.patch(`/users/${user.id}`, user)
}

function delUser(user: User) {
    return http.delete(`/users/${user.id}`)
}

function getUsers() {
    return http.get('/users')
}
function getUser(id: number) {
    return http.get(`/users/${id}`)
}

export default {
    addUser, updateUser, delUser, getUsers, getUser
}